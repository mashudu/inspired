package Step_Defination;

import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.google.gson.JsonObject;

import PageObject.Task2Objects;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import methods.methods;

public class Task2StepDefination extends methods{

	private static final String String = null;
	String propertyFilepath = "src/test/resources/Resource/config.properties";
	public String amountDeposit;
	public String amountWithdrawl;
	public String Amount;
	public String AmountBeforeWithdrawl;
    Scenario scenario;
	
	static JsonObject jsn = new JsonObject();
	

	@Before
	public void beforeStep(Scenario scenario) {
		this.scenario = scenario;

	}
	

	@Given("^The user has to go to the Bank xyz$")
	public void the_user_has_to_go_to_the_Bank_xyz() throws Throwable {
		webdriverOpen();
		driver.get(GetPropVal("EnviromentURl", propertyFilepath));
		
	      scenario.embed(getSc(driver), "image/png"); 
	}

	@When("^The user landed to the xyz Bank$")
	public void the_user_landed_to_the_xyz_Bank() throws Throwable {
		isElementVisible(Task2Objects.BankName_headers, "Bank Name") ;
	      scenario.embed(getSc(driver), "image/png");
	}

	@And("^The user login to the bank as Customer$")
	public void the_user_login_to_the_bank_as_Customer() throws Throwable {
		
		clickOnElement(Task2Objects.CustomerLogin_btn, "Customer login ");

	      scenario.embed(getSc(driver), "image/png");
	}

	@And("^Select the customer name \"([^\"]*)\"$")
	public void select_the_customer_name(String sName) throws Throwable {
		waitForElement(Task2Objects.SelectCustomer_Btn, 10);
		selectByVisibleText(Task2Objects.SelectCustomer_Btn, sName);
	      scenario.embed(getSc(driver), "image/png");
	}

	@And("^Click the login Button$")
	public void click_the_login_Button() throws Throwable {

		clickOnElement(Task2Objects.Login_btn, " login ");
	      scenario.embed(getSc(driver), "image/png");
	}

	@And("^Logout button should be visible$")
	public void logout_button_should_be_visible() throws Throwable {
		isElementVisible(Task2Objects.Logout_btn, "LogouT") ;
	      scenario.embed(getSc(driver), "image/png");
	}

	@And("^Select the account you may need \"([^\"]*)\"$")
	public void select_the_account_you_may_need(String sAccount) throws Throwable {
		waitForElement(Task2Objects.AccountNo_btn, 10);
		selectByVisibleText(Task2Objects.AccountNo_btn, sAccount); 
	      scenario.embed(getSc(driver), "image/png");
	}

	@And("^Click the Deposit Button$")
	public void click_the_Deposit_Button() throws Throwable {
		amountDeposit = getElementText(Task2Objects.Balance_txt);
		clickOnElement(Task2Objects.Deposit_btn, " Deposit ");
	      scenario.embed(getSc(driver), "image/png");

	}

	@And("^Enter the Amount \"([^\"]*)\" you need Deposit it$")
	public void enter_the_Amount_you_need_Deposit_it(String sAmount) throws Throwable {

		Amount=sAmount;
		Amount=GetPropVal("AmountDeposit", propertyFilepath);
		typeInTextBox(Task2Objects.DepositAmount_txt, Amount, "Amount"); 
	      scenario.embed(getSc(driver), "image/png");
	}

	@And("^Click the Depost Amount Button$")
	public void click_the_Depost_Amount_Button() throws Throwable {
		clickOnElement(Task2Objects.DepositButton_btn, " Deposit "); 
	      scenario.embed(getSc(driver), "image/png");
	}


	@And("^Click the Log out button$")
	public void click_the_Log_out_button() throws Throwable {
		clickOnElement(Task2Objects.Logout_btn, "Logout");   
	      scenario.embed(getSc(driver), "image/png");	}

	@And("^Click the Transcation Button$")
	public void click_the_Transcation_Button() throws Throwable {
		clickOnElement(Task2Objects.Transaction_btn, "Transaction");  
	      scenario.embed(getSc(driver), "image/png");
	}

	@And("^Verify if you have transcation for deposit$")
	public void verify_if_you_have_transcation_for_Deposit() throws Throwable {
		String TranscationAmont=getElementText(Task2Objects.Amount_ColumnValue);
		Assert.assertEquals(TranscationAmont, Amount);  
	      scenario.embed(getSc(driver), "image/png");
	}

	@And("^Click the back button$")
	public void click_the_back_button() throws Throwable {
		clickOnElement(Task2Objects.Back_btn, "Back"); 
	      scenario.embed(getSc(driver), "image/png");
	}

	@And("^Click the Withdrawl Button$")
	public void click_the_Withdrawl_Button() throws Throwable {
		AmountBeforeWithdrawl = getElementText(Task2Objects.Balance_txt);
		clickOnElement(Task2Objects.withdrawl_btn, "withdrawl"); 
	      scenario.embed(getSc(driver), "image/png");
		
	}

	@And("^Click the Withdrawl Amount Button$")
	public void click_the_Withdrawl_Amount_Button() throws Throwable {
		clickOnElement(Task2Objects.DepositButton_btn, "withdrawl Submit");   
	      scenario.embed(getSc(driver), "image/png");
	}

	@And("^Validate if you have withdrawl the amount \"([^\"]*)\"$")
	public void validate_if_you_have_withdrawl_the_amount(String sMessage) throws Throwable {
		String Message=getElementText(Task2Objects.SuccessfulWithdrawl);
		Assert.assertEquals(Message, sMessage);
	      scenario.embed(getSc(driver), "image/png");
	} 

	@And("^Verify if you have transcation for withdrawl$")
	public void verify_if_you_have_transcation_for_withdrawl() throws Throwable {

		String TranscationAmont=getElementText(Task2Objects.Amount_ColumnValue);
		Assert.assertEquals(TranscationAmont, amountWithdrawl); 
	      scenario.embed(getSc(driver), "image/png");
	}
	@And("^Enter the Deposited Amount \"([^\"]*)\" t$")
	public void enter_the_Deposited_Amount_t(String sAmount) throws Throwable {
		Amount=sAmount;
		Amount=GetPropVal("AmountDeposited", propertyFilepath);
		typeInTextBox(Task2Objects.DepositAmount_txt, Amount, "Amount");      
	      scenario.embed(getSc(driver), "image/png");
	}

	@And("^Enter the Withdrawl Amount \"([^\"]*)\" you need Deposit it$")
	public void enter_the_Withdrawl_Amount_you_need_Deposit_it(String sAmount) throws Throwable {
		amountWithdrawl=sAmount;
		amountWithdrawl=GetPropVal("AmountWithdrals", propertyFilepath);
		typeInTextBox(Task2Objects.DepositAmount_txt, Amount, "Amount");  
	      scenario.embed(getSc(driver), "image/png");
	}
	@When("^Select the customer name$")
	public void select_the_customer_name() throws Throwable {
		jsn = convertFileToObject("src/test/resources/json Files/payloads/customerDeposit.Json");
		
		
		String CustomerName = deleteFirstAndLastChar(jsn.getAsJsonObject().get("CustomerName").toString());
		waitForElement(Task2Objects.SelectCustomer_Btn, 10);
		selectByVisibleText(Task2Objects.SelectCustomer_Btn, CustomerName);
		
	
		

	    String DepositAmount= jsn.getAsJsonObject().get("DepositAmount").toString() ;
	    
	  
		
		}

	@When("^Select the account you may need$")
	public void select_the_account_you_may_need() throws Throwable {
		
		String accountNo =deleteFirstAndLastChar(jsn.getAsJsonObject().get("AccountNo").toString()) ;
		waitForElement(Task2Objects.AccountNo_btn, 10);
		selectByVisibleText(Task2Objects.AccountNo_btn, accountNo); 
	      scenario.embed(getSc(driver), "image/png");
	      
	}

	@When("^Enter the Deposited Amount$")
	public void enter_the_Deposited_Amount() throws Throwable {
	 
		String DepositAmount= jsn.getAsJsonObject().get("DepositAmount").toString() ;
		typeInTextBox(Task2Objects.DepositAmount_txt, DepositAmount, "Amount"); 
	      scenario.embed(getSc(driver), "image/png");
	      
	}

	@When("^Enter the Withdrawl Amount you need Deposit it$")
	public void enter_the_Withdrawl_Amount_you_need_Deposit_it() throws Throwable {
		 
		String withdrawalAmount = jsn.getAsJsonObject().get("withdrawalAmount").toString();
		waitForElement(Task2Objects.DepositAmount_txt, 60);
		typeInTextBox(Task2Objects.DepositAmount_txt, withdrawalAmount, "Amount");
		typeInTextBox(Task2Objects.DepositAmount_txt, withdrawalAmount, "Amount"); 
		 
	    scenario.embed(getSc(driver), "image/png");
		
	}
	@After
	public static WebDriver tearDown() {

		try {

			driver.quit();
			System.out.println("Browser closed successful");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return driver;
	}

}
