package Step_Defination;

import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.junit.Assert;

import com.google.gson.JsonObject;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import methods.methods;

public class Task1StepDefination extends methods  {

	methods obj = new methods();

	String jsonFilePath = "src/test/resources/json Files/Payloads/addNewUser.Json";
	String adduserResPath = "src/test/resources/json Files/responses/addUser.json";
	String allUsersResPath = "src/test/resources/json Files/responses/allUsers.json";
	String propertyFilepath = "src/test/resources/Resource/config.properties";
	String singleUserResPath = "src/test/resources/json Files/responses/singleUser.json";
	Response res;
	public static Scenario scenario;
	int statusCode = 0;
	JsonObject jsonObj = null;


	@Before
	public void beforeStep(Scenario scenario) throws ParseException {
		this.scenario = scenario;
		 this.jsonObj = convertFileToObject(adduserResPath);
	}

	@Given("^the user details to be sent are not null$")
	public void the_user_details_to_be_sent_are_not_null() throws Throwable {
		
		 
		 scenario.write("Find payload below \n"+jsonObj.toString());
		 
		 Assert.assertNotNull("The json object/ file is empty", jsonObj);
		 
		
		 
	}

	
	@When("^a request is send the the /users endpoint$")
	public void a_request_is_send_the_the_users_endpoint() throws Throwable {


		//sending request
		res= PostRequest(GetPropVal("BaseURL", propertyFilepath),GetPropVal("userEndpoint", propertyFilepath),jsonFilePath);
		 scenario.write("Find response below \n"+res.asString());
		 Assert.assertNotNull("The response is Empty/ null", res);
	}

	@When("^The status code return is Created$")
	public void the_status_code_return_is_Created() throws Throwable {
	  
		 statusCode= res.getStatusCode();
		
		 scenario.write("Status code is \n"+statusCode);

		 Assert.assertEquals(201, statusCode);
		
		
		
	}
	@Then("^Validate the response$")
	public void validate_the_response() throws Throwable {

		compareResponseAndExpectedforUser(jsonObj, res);
		 
	} 
	
	@When("^a request is send the the /users endpoint to retrive user with the id \"([^\"]*)\"$")
	public void a_request_is_send_the_the_users_endpoint_to_retrive_user_with_the_id(String arg1) throws IOException {
	    
		res= GetRequest(GetPropVal("BaseURL", propertyFilepath), GetPropVal("userEndpoint", propertyFilepath), arg1);
		
	}

	@When("^The status code return is OK$")
	public void the_status_code_return_is_OK() throws Throwable {
	   
		statusCode= res.getStatusCode();
		
		 scenario.write("Status code is \n"+statusCode);

		 Assert.assertEquals(200, statusCode);
	}

	@Then("^Validate if the endpoint returned correct user details$")
	public void validate_if_the_endpoint_returned_correct_user_details() throws Throwable {
	    
		
		
		jsonObj = convertFileToObject(singleUserResPath) ;
		
		scenario.write("Response=  \n"+res.asString()); 
		compareResponseAndExpectedforUser(jsonObj, res);
	}
	
	@When("^a request is send the the /users endpoint to retive all users$")
	public void a_request_is_send_the_the_users_endpoint_to_retive_all_users() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		
		res= GetRequest(GetPropVal("BaseURL", propertyFilepath), GetPropVal("userEndpoint", propertyFilepath),"");
	
		
	   
	}

	@Then("^Validate if all users are returned$")
	public void validate_if_all_users_are_returned() throws Throwable {
		
		scenario.write("Response=  \n"+res.asString()); 
	    
	}
}




