package methods;

import static io.restassured.RestAssured.given;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;


public class methods {
	 
	 public static WebDriver driver;
	
	public Response  PostRequest(String baseUrl, String endPoint, String jsonFilePath) {
		
		RestAssured.baseURI= baseUrl;

		Response Res=given().header("Content-Type","application/json")
					 .body(new File(jsonFilePath)).when().post(endPoint)
					 .then().extract().response();
		
		return Res;
		
	}
	
	public Response  GetRequest(String baseUrl, String endPoint, String UniqueID) {
		

			RestAssured.baseURI= baseUrl;

			Response Res=given().header("Content-Type","application/json")
						 .when()
						 .get(endPoint+UniqueID)
						 .then()
						 .extract()
						 .response();
			
			return Res;
			
		}
	
	public String ValidateResAgainsExpected( Response res, String ResFieldName ,JSONObject jsonObj, String jsnFieldName, int IterationCircle ) {
		     String jsonValue = null;
		     String responseValue = null;
		     
		     if(IterationCircle== 1) {
		    	 
		    	 responseValue= res.jsonPath().getString(ResFieldName);
			     jsonValue = jsonObj.get(jsnFieldName).toString(); 
			     
		     	}
		     else if(IterationCircle== 2) {
		    	 
		    	 Response res1 = null;
		    	 JSONObject JSojson1 = null;
		    	 
		    	 
		    	 
		    	 responseValue= res.jsonPath().getString(ResFieldName);
			     jsonValue = jsonObj.get(jsnFieldName).toString(); 
		    	 
		    	 
		     }
		
		
		   
		return jsonValue + " hell "+  responseValue;
	}
	 
 
	public String GetPropVal(String propertyRetrived, String propFilePath) throws IOException {
		String val = null; 

		try (InputStream input = new FileInputStream(propFilePath)) {

			Properties prop = new Properties();
			prop.load(input);
			val = prop.getProperty(propertyRetrived);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return val;  
	}
	
	@SuppressWarnings("deprecation")
	public JsonObject  convertFileToObject(String jsonFilePath ) throws ParseException {
		
			JsonParser parser = new JsonParser();
		
		try {
			Object object = parser.parse(new FileReader(jsonFilePath));
            
            //convert Object to JSONObject
            JsonObject jsonObject = (JsonObject)object;
	        
            return jsonObject;
            
	    } catch (IOException  e) {
	        e.printStackTrace();
	        
	        return null;
	        
	    }
	
	}
	public boolean nullcheck(String val) {
		
		boolean returnValue;
		if(val.trim().isEmpty() || val == null ) {
			
			returnValue = false;
			
		}
		else {
			returnValue = true;
			
		}
		
		return returnValue;
	}
   
	public String  getStringFromObject(JsonObject jsonObj , String objPath) {
		
		return jsonObj.get(objPath).toString();
	}
	
public String  getStringFromResponse(Response res , String Path) {
		
		return res.jsonPath().getString(Path);
		
	}


public JsonObject  convertResponseToObject(Response res) {
	
	@SuppressWarnings("deprecation")
	JsonObject jsonObject = new JsonParser().parse(res.asString()).getAsJsonObject();
	
	return jsonObject;
	  
}

public String deleteFirstAndLastChar(String str) {
	String subString = null;
	subString = str.substring(0);
	subString = subString.substring(1, str.length()-1);
	
	
	return subString;
	
}
	
	
public void compareResponseAndExpectedforUser(JsonObject jsonObj, Response res) {
	
	   JsonObject responseObject1=  convertResponseToObject(res); 

	    String id = getStringFromObject( jsonObj , "id");
	    String name = deleteFirstAndLastChar(getStringFromObject( jsonObj , "name")) ;
	    String username= deleteFirstAndLastChar(getStringFromObject( jsonObj , "username")) ;
	    String email =  deleteFirstAndLastChar(getStringFromObject( jsonObj , "email"));
		String phone = deleteFirstAndLastChar(getStringFromObject( jsonObj , "phone"));
		
		
		String suiteFile = deleteFirstAndLastChar(jsonObj.getAsJsonObject("address").get("suite").toString());
		String suiteResponse = deleteFirstAndLastChar(responseObject1.getAsJsonObject("address").get("suite").toString());
		
		String streetFile = deleteFirstAndLastChar(jsonObj.getAsJsonObject("address").get("street").toString());
		String streetResponse = deleteFirstAndLastChar(responseObject1.getAsJsonObject("address").get("street").toString());
		
		String cityFile = deleteFirstAndLastChar(jsonObj.getAsJsonObject("address").get("city").toString());
		String cityResponse = deleteFirstAndLastChar(responseObject1.getAsJsonObject("address").get("city").toString());
		
		String zipcodeFile = deleteFirstAndLastChar(jsonObj.getAsJsonObject("address").get("zipcode").toString());
		String zipcodeResponse = deleteFirstAndLastChar(responseObject1.getAsJsonObject("address").get("zipcode").toString());
		
		String latFile = deleteFirstAndLastChar(jsonObj.getAsJsonObject("address").getAsJsonObject("geo").get("lat").toString());
		String latResponse = deleteFirstAndLastChar(responseObject1.getAsJsonObject("address").getAsJsonObject("geo").get("lat").toString());
		
		
		String CompayNameFile = deleteFirstAndLastChar(jsonObj.getAsJsonObject("company").get("name").toString());
		String CompanyNameResponse = deleteFirstAndLastChar(responseObject1.getAsJsonObject("company").get("name").toString());
		
		String catchPhraseFile = deleteFirstAndLastChar(jsonObj.getAsJsonObject("company").get("catchPhrase").toString());
		String catchPhraseResponse = deleteFirstAndLastChar(responseObject1.getAsJsonObject("company").get("catchPhrase").toString());
		
		String bsFile = deleteFirstAndLastChar(jsonObj.getAsJsonObject("company").get("bs").toString());
		String bsResponse = deleteFirstAndLastChar(responseObject1.getAsJsonObject("company").get("bs").toString());
		
		
		 
				
			Assert.assertTrue(getStringFromResponse( res ,"id").equalsIgnoreCase(id) );
			// validating the name
			Assert.assertTrue(getStringFromResponse( res ,"name").equalsIgnoreCase(name) );
			// validating username
		    Assert.assertTrue(getStringFromResponse( res ,"username").equalsIgnoreCase(username));
			
			// validating email
			Assert.assertTrue(getStringFromResponse( res ,"email").equalsIgnoreCase(email) );
			// validating phone
			Assert.assertTrue(getStringFromResponse( res ,"phone").equalsIgnoreCase(phone) );
		
			// validating suite
			Assert.assertTrue(suiteFile.equalsIgnoreCase(suiteResponse));
			
			
			// validating the city
			Assert.assertTrue(cityFile.equalsIgnoreCase(cityResponse) );
			
			// validating zip code
			Assert.assertTrue(zipcodeFile.equalsIgnoreCase(zipcodeResponse));
			// validating latitude
			Assert.assertTrue(latFile.equalsIgnoreCase(latResponse) );
			
			// validating the company name
			
			Assert.assertTrue(CompayNameFile.equalsIgnoreCase(CompanyNameResponse) );
			// validating  the catch phrase
			Assert.assertTrue(catchPhraseFile.equalsIgnoreCase(catchPhraseResponse));
			
			// validating 
			Assert.assertTrue(bsFile.equalsIgnoreCase(bsFile) );
				
	

}

/*
 * 
 * MASHUDU
 * 
 * 
 */
	


public static void webdriverOpen() {
	System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
	driver = new ChromeDriver();
    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	driver.manage().deleteAllCookies();
	driver.manage().window().maximize();
}
public static void waitForElement(By Locator, long lTime) throws IOException {
	WebDriverWait wait = new WebDriverWait(driver, lTime);
	wait.until(ExpectedConditions.elementToBeClickable(Locator));
}
public static boolean clickOnElement(By object, String elementName) throws Exception {
	boolean bFlag = false;
	try {
		if (driver.findElement(object).isDisplayed()) {
			driver.findElement(object).click();
			bFlag = true;
		} else {
			System.out.println("Failed to click on " + elementName);
		}
	} catch (Exception ex) {
		//GetSreenshot(driver);
		ex.printStackTrace();
	}
	return bFlag;
	
}

public static boolean isElementVisible(By object, String elementName) throws Exception {
	boolean bFlag = false;
	try {
		driver.manage().timeouts().implicitlyWait(3000,TimeUnit.MILLISECONDS);
		if (driver.findElement(object).isDisplayed()) {
			bFlag = true;
			System.out.println("Element " + elementName + " is visible on screen");
		} else {
			driver.manage().timeouts().implicitlyWait(3000,TimeUnit.MILLISECONDS);
			System.out.println("Element " + elementName + " is not visible on screen");
		}

	} catch (Exception ex) {
		GetSreenshot(driver);
		ex.printStackTrace();
	}
	return bFlag;
}

public static void selectByVisibleText(By objLocator, String sVisibletext) throws Throwable {

	try {
		Select s = new Select(driver.findElement(objLocator));
		s.selectByVisibleText(sVisibletext);

	} catch (Exception ex) {
		GetSreenshot(driver);
		ex.printStackTrace();
	}
}
	public static String getElementText(By locator) throws Throwable {
		String text = "";
		try {
			if (isElementVisible(locator, "Element")) {
				text = driver.findElement(locator).getText();
			}
		}  catch (Exception ex) {
			GetSreenshot(driver);
			ex.printStackTrace();
		}
		return text;

	}
	public static void typeInTextBox(By object, String data, String elementName) throws Exception {
		try {
			if (driver.findElement(object).isDisplayed()) {
				driver.findElement(object).clear();
				driver.findElement(object).sendKeys(data);
				System.out.println("Entered data in " + elementName + " textbox");
			} else {
				System.out.println("Failed to enter data in " + elementName + " textbox");
			}
		} catch (Exception ex) {
			GetSreenshot(driver);
			ex.printStackTrace();
		}}
	
	
	public static String GetSreenshot(WebDriver driver) throws Exception
	{
	TakesScreenshot ts=(TakesScreenshot) driver;

	File src=ts.getScreenshotAs(OutputType.FILE);

	String path="./Screenshots/"+System.currentTimeMillis()+".png";

	File destination=new File(path);

	try
	{
	FileUtils.copyFile(src, destination);
	} catch (IOException e)
	{
	System.out.println("Capture Failed "+e.getMessage());
	}

	return path;
	}
public static byte[] getSc(WebDriver driver) {
	
    // Take a screenshot...
      byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
      
      return screenshot;
   // embed it in the report.
}



}
	
