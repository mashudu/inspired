package PageObject;

import org.openqa.selenium.By;

public class Task2Objects {
	public static final By CustomerLogin_btn=By.xpath("//button[@class='btn btn-primary btn-lg']");
	public static final By BankName_headers=By.xpath("//strong[@class='mainHeading']");
	public static final By SelectCustomer_Btn=By.xpath("//select[@id='userSelect']");
	public static final By SelectCustomer_ddl=By.xpath("//select[@id='userSelect']//option");
	public static final By Login_btn=By.xpath("//button[@type='submit']");
	public static final By Logout_btn=By.xpath("//button[@class='btn logout']");
	public static final By Balance_txt=By.xpath("(//strong[@class='ng-binding'])[2]");
	public static final By Transaction_btn=By.xpath("//button[contains(text(),'Transactions')]");
	public static final By Deposit_btn=By.xpath("//button[contains(text(),'Deposit')]");
	public static final By withdrawl_btn=By.xpath("//button[contains(text(),'Withdrawl')]");
	public static final By AccountNo_btn=By.xpath("//select[@id='accountSelect']");
	public static final By AccountNo_ddl=By.xpath("//select[@id='accountSelect']//option");
	public static final By Amount_ColumnValue=By.xpath("//td[@class='ng-binding'][2]");
	public static final By DepositAmount_txt=By.xpath("//input[@placeholder='amount']");
	public static final By DepositButton_btn=By.xpath("//button[@type='submit']");
	public static final By WithdtrawSubmit_btn=By.xpath("//button[contains(text(),'Withdraw')]");
	public static final By Back_btn=By.xpath("//button[@class='btn']");
	public static final By SuccessfulWithdrawl=By.xpath("//span[@class='error ng-binding']");
	

}
