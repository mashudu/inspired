Feature: fetaure to add new and retrieve users uding the /users EndPoint

Background: 
Given The user has to go to the Bank xyz
When The user landed to the xyz Bank

@Working
Scenario: Test1 login as customer and deposit at the first account
And The user login to the bank as Customer
And Select the customer name "Harry Potter"
And Click the login Button
And Logout button should be visible 
And Select the account you may need "1004"
And Click the Deposit Button
And Enter the Amount "AmountDeposit" you need Deposit it
And Click the Depost Amount Button
And Validate if you have withdrawl the amount "Deposit Successful"
And Click the Log out button

@Working
Scenario: Test2 login as customer and deposit at the at each account
And The user login to the bank as Customer
And Select the customer name "Ron Weasly"
And Click the login Button
And Logout button should be visible 
And Select the account you may need "1007"
And Click the Deposit Button
And Enter the Amount "AmountDeposit" you need Deposit it
And Click the Depost Amount Button
And Validate if you have withdrawl the amount "Deposit Successful"
And Select the account you may need "1008"
And Click the Deposit Button
And Enter the Amount "AmountDeposit" you need Deposit it
And Click the Depost Amount Button
And Validate if you have withdrawl the amount "Deposit Successful"
And Select the account you may need "1009"
And Click the Deposit Button
And Enter the Amount "AmountDeposit" you need Deposit it
And Click the Depost Amount Button
And Validate if you have withdrawl the amount "Deposit Successful"
And Click the Log out button

@Working
Scenario: Test3 login as customer and deposit on the first account 
And The user login to the bank as Customer
And Select the customer name "Albus Dumbledore"
And Click the login Button
And Logout button should be visible 
And Select the account you may need "1010"
And Logout button should be visible
And Click the Deposit Button
And Enter the Deposited Amount "AmountDeposit" t
And Click the Depost Amount Button
And Validate if you have withdrawl the amount "Deposit Successful"
And Click the Transcation Button 
And Verify if you have transcation for deposit
And Click the back button
And Click the Withdrawl Button
And Enter the Withdrawl Amount "AmountWithdrawl" you need Deposit it
And Click the Withdrawl Amount Button  
And Validate if you have withdrawl the amount "Transaction successful"
And Click the Transcation Button
And Verify if you have transcation for withdrawl
And Click the back button
And Click the Log out button

@NotWorking
Scenario: Test4 login as customer and deposit on the first account using Json file
And The user login to the bank as Customer
And Select the customer name 
And Click the login Button
And Logout button should be visible 
And Select the account you may need 
And Logout button should be visible
And Click the Deposit Button
And Enter the Deposited Amount 
And Click the Depost Amount Button
And Validate if you have withdrawl the amount "Deposit Successful"
And Click the Withdrawl Button
And Enter the Withdrawl Amount you need Deposit it
And Click the Withdrawl Amount Button  
And Validate if you have withdrawl the amount "Transaction successful"
And Click the Log out button

