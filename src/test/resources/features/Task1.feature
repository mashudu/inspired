Feature: fetaure to add new and retrieve users uding the /users EndPoint

Scenario: Add new user to the endpoint /users and validate if the response is correct

Given the user details to be sent are not null
When  a request is send the the /users endpoint
And The status code return is Created
Then Validate the response

Scenario: retrive a single user with id   and validate if the response is correct

When  a request is send the the /users endpoint to retrive user with the id "2"
And The status code return is OK
Then Validate if the endpoint returned correct user details

Scenario: retrive all users 

When  a request is send the the /users endpoint to retive all users
And The status code return is OK
Then Validate if all users are returned